<?php
//Pour pouvoir me servir des sessions
session_start();
include "header.php";
mon_header("Ajouter une espèce");

?>

<h1>Ajouter une espèce :</h1>

<form method="post" action="actions/actionAjout_Espèce.php">
    <div class="form-group">
        <input type="text" class="form-control" id="nom" maxlength="50" name="nom" placeholder="Nom de l'espèce a ajouter..." required>
    </div>
<button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
</form>


<?php
include 'footer.php';
mon_footer();
?>
