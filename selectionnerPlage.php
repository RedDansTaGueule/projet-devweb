<?php
include "header.php";
mon_header("Sélection de la plage");
session_start();
?>
<?php

$idetude = $_SESSION["idetude"];
require "config.php";
//créer l'objet PDO qui me connecte à la bdd
$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete=$db->prepare("select ep.idetude, ep.idplage, p.nom from etude_plage ep, plage p where idetude=:idetude and ep.idplage=p.id");
$requete->bindParam("idetude",$idetude);

$requete->execute();

$plages =$requete->fetchALL();
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container3">
        <h1 class="display-4">Paramètres du prélèvement</h1>
    </div>
</div>
<table class="table table-striped">
    <tr>
        <th>Plages</th>
        <th></th>
        <th></th>
    </tr>
</table>
</div>

<form action="selectionnerGroupe.php" method="post">
    <label for="plage">Sélectionnez une plage</label>
    <select name="idplage" class="form-control">
        <?php
        foreach($plages as $plage){
            ?>
            <option value="<?php echo $plage["idplage"] ?>">
                <?php echo $plage["nom"] ?>
            </option>
            <?php
        }
        ?>
    </select>
    <input type="submit" class="btn btn-info"/>
</form>


