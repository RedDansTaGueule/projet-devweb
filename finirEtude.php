<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Finir une étude");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;

$id=filter_input(INPUT_GET,"id");

require "config.php";
$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );
$requete = $db->prepare("select id, datefin from etude where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();
$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !";
    http_response_code(404);
    include "footer.php";
    die();
}
$datefin=$lignes[0]["datefin"];
?>

<h1>Finir une étude</h1>

<form method="post" action="Actions/actionFinirEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="id" value="<?php echo $id ?>">

    <div class="form-group">
        <label for="datefin">Date de fin de l'étude</label>
        <input type="date" class="form-control" id="datefin" name="datefin" placeholder="Date de fin" required>
    </div>

    <a href="modifierEtude.php" class="btn btn-danger">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>

    <button type="submit" class="btn btn-primary pull-right">Finir étude</button>
</form>

<?php include "footer.php";
mon_footer(); ?>