<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Modifier Plage");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;

$id=filter_input(INPUT_GET, "id");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select id, nom from plage where id=:id");

$requete->bindParam(":id", $id);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !";
    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}
$id=$lignes[0]["id"];
$nom=$lignes[0]["nom"];
?>

<h1>Modifier une plage</h1>

<form method="post" action="actions/actionEditPlage.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="id" value="<?php echo $id ?>">
    <div class="form-group">
        <label for="nom">Nom de la plage</label>
        <input type="text" class="form-control" id="nom" maxlength="50" name="nom" value="<?php echo $nom ?>" placeholder="Nom de la plage" required>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Enregistrer modifications</button>
</form>

<br>

<?php include "footer.php";
mon_footer(); ?>
