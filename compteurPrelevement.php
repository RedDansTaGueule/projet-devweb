<?php
include "header.php";
require "config.php";

mon_header("Recensement des espèces :");
session_start();


//créer l'objet PDO qui me connecte à la bdd
$db = new PDO("mysql:host=" . config::SERVEUR . ";dbname=" . config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete = $db->prepare("select distinct e.id, e.nom, ep.nmbresp as nmbresp from espece e left join espece_prelevement ep on e.id=ep.idespece and ep.idprelevement = :idprelevement");
$requete->bindParam("idprelevement",$_SESSION["idprelevement"]);
$requete->execute();
$especes = $requete->fetchALL();
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container3">
        <h1 class="display-4">Prélèvement</h1>
    </div>
</div>
<table class="table table-striped">
    <tr>
        <th>ID</th>
        <th>Nom espèce</th>
        <th>Nombre</th>
    </tr>

<?php
    foreach ($especes
        as $espece){

    ?>
        <form action="actions/actionQuantité.php" method="post" id="formeu">
    <tr>
            <td><?php echo $espece["id"] ?></td>
            <td><?php echo $espece["nom"] ?></td>
            <td>
                    <input type="number" name="nmbresp" min="0" id="nmbresp" value="<?php echo $espece["nmbresp"] ?>"/>
                <input type="hidden" name="idespece" id="idespece" value=<?php echo $espece ["id"] ?>>
                    <input type="hidden" name="idprelevement" value=<?php echo $_SESSION["idprelevement"] ?>>
            </td>
            <td>
            </td>
    </tr>
        </form>
        <?php
    }
    ?>
</table>
<input type="submit" form="formeu">
