<?php
session_start();
require "../config.php";

$lat1=filter_input(INPUT_POST, "lat1");
$lon1=filter_input(INPUT_POST, "lon1");
$lat2=filter_input(INPUT_POST, "lat2");
$lon2=filter_input(INPUT_POST, "lon2");

$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

//préparer une requête
$requete=$db->prepare("insert into zone(lat1, lon1, lat2, lon2)"." values (:lat1, :lon1, :lat2, :lon2)");
$requete->bindParam(":lat1", $lat1);
$requete->bindParam(":lon1", $lon1);
$requete->bindParam(":lat2", $lat2);
$requete->bindParam(":lon2", $lon2);

$requete->execute();

$requete=$db->prepare("SELECT LAST_INSERT_ID();");
$requete->execute();

$id = $requete->fetch();

$_SESSION["idzone"] = $id[0];
$_SESSION["lat1"] = $lat1;
$_SESSION["lon1"] = $lon1;
$_SESSION["lat2"] = $lat2;
$_SESSION["lon2"] = $lon2;


header("location: ../compteurPrelevement.php");

