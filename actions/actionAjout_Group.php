<?php


session_start();

$nomgrp = filter_input(INPUT_POST, "nomgrp");
$nmbrpers = filter_input(INPUT_POST, "nmbrpers");

require "../config.php";


$db = new PDO("mysql:host=" . config::SERVEUR . ";dbname=" . config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

//préparer une requête
$requete = $db->prepare("insert into groupe(nomgrp, nmbrpers)" . " values (:nomgrp, :nmbrpers)");

$requete->bindParam(":nomgrp", $nomgrp);
$requete->bindParam(":nmbrpers", $nmbrpers);

$requete->execute();

header("location: ../selectionnerGroupe.php");

