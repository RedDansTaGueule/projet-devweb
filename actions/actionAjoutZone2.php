<?php
session_start();
require "../config.php";

$lat3=filter_input(INPUT_POST, "lat3");
$lon3=filter_input(INPUT_POST, "lon3");
$lat4=filter_input(INPUT_POST, "lat4");
$lon4=filter_input(INPUT_POST, "lon4");
$id=filter_input(INPUT_POST, "id");

$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

//préparer une requête
$requete=$db->prepare("UPDATE zone z  SET `lat3`=:lat3,`lon3`=:lon3,`lat4`=:lat4,`lon4`=:lon4 WHERE z.id=:z.id");
$requete->bindParam(":lat3", $lat3);
$requete->bindParam(":lon3", $lon3);
$requete->bindParam(":lat4", $lat4);
$requete->bindParam(":lon4", $lon4);
$requete->bindParam(":id", $id);

$requete->execute();

$requete=$db->prepare("SELECT LAST_INSERT_ID();");
$requete->execute();

$id = $requete->fetch();
var_dump($_SESSION);
$_SESSION["lat3"] = $lat3;
$_SESSION["lon3"] = $lon3;
$_SESSION["lat4"] = $lat4;
$_SESSION["lon4"] = $lon4;


header("location: ../actions/actionCalculDist.php");

