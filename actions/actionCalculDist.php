<?php


session_start();

require "../config.php";

var_dump($_SESSION);
function get_distance_m($lat1, $lon1, $lat2, $lon2) {
    $earth_radius = 6378137;
    $rlo1 = deg2rad($lon1);
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lon2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
                $dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return ($earth_radius * $d);
}
function get_distance_mn($lat3, $lon3, $lat4, $lon4) {
    $earth_radius = 6378137;
    $rlo1 = deg2rad($lon3);
    $rla1 = deg2rad($lat3);
    $rlo2 = deg2rad($lon4);
    $rla2 = deg2rad($lat4);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
                $dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return ($earth_radius * $d);
}

echo (round(get_distance_mn($_SESSION["lat3"], $_SESSION["lon3"], $_SESSION["lat4"], $_SESSION["lon4"]) / 1000,
        3)). ' km';

echo (round(get_distance_m($_SESSION["lat1"], $_SESSION["lon1"], $_SESSION["lat2"], $_SESSION["lon2"]) / 1000,
        3)). ' km';

$longueur1=(round(get_distance_m($_SESSION["lat1"],$_SESSION["lon1"],$_SESSION["lat2"],$_SESSION["lon2"]) / 1000, 3)) ;
$longueur2=(round(get_distance_mn($_SESSION["lat3"],$_SESSION["lon3"],$_SESSION["lat4"],$_SESSION["lon4"]) / 1000, 3)) ;
$surfaceZone=$longueur1*$longueur2;

$_SESSION["surfaceZone"]=$surfaceZone;

header("location: ../AffichageSuperficie.php");
?>
