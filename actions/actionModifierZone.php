<?php

require "../config.php";

$lat1=filter_input(INPUT_POST, "lat1");
$lon1=filter_input(INPUT_POST, "lon1");
$lat2=filter_input(INPUT_POST, "lat2");
$lon2=filter_input(INPUT_POST, "lon2");
$id=filter_input(INPUT_POST,"id");

$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

//préparer une requête
$requete=$db->prepare("insert into zone(lat1, lon1, lat2, lon2)"." values (:lat1, :lon1, :lat2, :lon2)");
$requete->bindParam(":lat1", $lat1);
$requete->bindParam(":lon1", $lon1);
$requete->bindParam(":lat2", $lat2);
$requete->bindParam(":lon2", $lon2);

$requete->execute();


header("location: ../compteurPrelevement.php");

