<?php

require "../config.php";
session_start();

$idetude=filter_input(INPUT_POST, "idetude");


$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

//préparer une requête
$requete=$db->prepare("insert into prelevement(idetude) values (:idetude)");
$requete->bindParam(":idetude", $idetude);
$requete->execute();

$requete=$db->prepare("SELECT LAST_INSERT_ID();");
$requete->execute();

$id = $requete->fetch();

$id = $id[0];

$_SESSION["idprelevement"] = $id;
$_SESSION["idetude"] = $idetude;
header("location: ../selectionnerPlage.php");

