<?php


session_start();

$nom=filter_input(INPUT_POST, "nom");
$id=filter_input(INPUT_GET, "id");
require "../config.php";

$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete=$db->prepare("delete from espece where id=:id");
$requete->bindParam(":id", $id);

$requete->execute();

//puis je vais retourner à l'accueil (la liste des catégories)
header("location: ../listeEspece.php");

