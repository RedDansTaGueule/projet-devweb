<?php
    include "header.php";
    mon_header("Liste des espèces :");
    ?>
    <div class="jumbotron jumbotron-fluid">
        <div class="container3">
            <h1 class="display-4">Prélèvement</h1>
        </div>
    </div>
    <table class="table table-striped">
        <tr>
            <th>Espèce</th>
            <th></th>
            <th>Modifier espèce</th>
        </tr>

        <a href="ajouterUneEspèce.php"><button type="button"  class="btn btn-success">Ajouter une espèce </button></a>
        <?php
        require "config.php";
        //créer l'objet PDO qui me connecte à la bdd
        $db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

        $requete=$db->prepare("select id, nom from espece");

        $requete->execute();

        $lignes =$requete->fetchALL();

        foreach ($lignes as $ligne){
            ?>
            <tr>
                <td><?php echo $ligne["nom"]?></td>
                <td></td>
                <td>
                    <a href="modifierEspece.php?id=<?php echo $ligne["id"]?>"
                       class="btn btn-primary"><i class="fa fa-pen"></i></a>
                    <a href="actions/actionDelete.php?id=<?php echo $ligne["id"] ?>"
                    class="btn btn-danger pull-right"><i class="fa fa-trash"></i> </a>
                </td>
            </tr>
            <?php
        }
        ?>

