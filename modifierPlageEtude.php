<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Modifier Plage");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;

$idplage=filter_input(INPUT_GET, "idplage");
$idetude=filter_input(INPUT_GET, "idetude");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select p.nom as plage, e.nom as etude, ep.superficie from plage p join etude_plage ep on p.id=ep.idplage join etude e on ep.idetude=e.id where p.id=:idplage AND e.id=:idetude");

$requete->bindParam(":idplage", $idplage);
$requete->bindParam(":idetude", $idetude);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !";
    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}
$plage=$lignes[0]["plage"];
$etude=$lignes[0]["etude"];
$superficie=$lignes[0]["superficie"];
?>

<h1>Modifier une plage</h1>

<form method="post" action="actions/actionEditPlageEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="idetude" value="<?php echo $idetude ?>">
    <input type="hidden" name="idplage" value="<?php echo $idplage ?>">
    <div class="form-group">
        <label for="superficie">Superficie de <b><?php echo $plage ?></b> dans <b><?php echo $etude ?></b></label>
        <input type="number" class="form-control" id="superficie" name="superficie" value="<?php echo $superficie ?>" placeholder="superficie" required>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Enregistrer modifications</button>
</form>

<br>

<?php include "footer.php";
mon_footer(); ?>
