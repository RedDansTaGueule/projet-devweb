<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Supprimer une Plage");

$token = rand(0, 2000000000);
$_SESSION["token"] = $token;

$idetude = filter_input(INPUT_GET, "idetude");
$idplage = filter_input(INPUT_GET, "idplage");
require "config.php";

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select p.id as idplage, p.nom as plage, e.nom as etude from plage p join etude_plage ep on p.id=ep.idplage join etude e on ep.idetude=e.id where p.id=:idplage");

$requete->bindParam("idplage", $idplage);
$requete->execute();
$lignes = $requete->fetchAll();

$plage = $lignes[0]["plage"];
$etude = $lignes[0]["etude"];
?>

<h1>Supprimer une plage</h1>

    <h2>Etes-vous sûr de vouloir supprimer la plage <b><?php echo $plage ?></b> de l'étude <b><?php echo $etude ?></b> ?</h2>

<form method="post" action="actions/actionDelPlageEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="idplage" value="<?php echo $idplage ?>">
    <input type="hidden" name="idetude" value="<?php echo $idetude ?>">

    <a href="listePlages.php" class="btn btn-primary">
        <i class="fa fa-long-arrow-left"></i>
        Retour</a>

    <button type="submit" class="btn btn-danger pull-right"><i class="fal fa-trash"></i> Supprimer</button>
</form>

<br>

<?php include "footer.php";
mon_footer(); ?>