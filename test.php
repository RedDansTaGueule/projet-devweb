<?php

session_start();

function surface($L,$l)
{
    $surface=$L*$l;
// Return permet de retourner la valeur que contient la fonction comme parametre
    return $surface;
}
// Appel de la fonction ainsi que les valeurs en parametre
$surface=surface(5,8);
// Affichage du resultat en faisant appel a la fonction
echo "<br/> La Surface du rectangle est:".$surface;
?>
<?php
//calcul de la distance 3D conçue par partir-en-vtt.com

class Misc {
    /**
     * Retourne la distance en metre ou kilometre (si $unit = 'k') entre deux latitude et longitude fournit
     */
    public static function distance($lat1, $lng1, $lat2, $lng2, $unit = 'k') {
        $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        $rlo1 = deg2rad($lng1);
        $rla1 = deg2rad($lat1);
        $rlo2 = deg2rad($lng2);
        $rla2 = deg2rad($lat2);
        $dlo = ($rlo2 - $rlo1) / 2;
        $dla = ($rla2 - $rla1) / 2;
        $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
        $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
        //
        $meter = ($earth_radius * $d);
        if ($unit == 'k') {
            return $meter / 1000;
        }
        return $meter;
    }
}
echo round(Misc::distance(48.86417880,2.34250440,43.6008177,3.8873392), 3);