<?php

include "header.php";
mon_header("IFROCEAN - Ajouter Plage");


$idetude = filter_input(INPUT_GET, "idetude");

require "config.php";

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select distinct p.nom, p.id as idplage from plage p
                                   join etude_plage ep on p.id=ep.idplage
                                   where (ep.idplage not in (select p.id from plage p join etude_plage ep on p.id=ep.idplage where ep.idetude=:idetude))");
$requete->bindParam(":idetude", $idetude);
$requete->execute();

$lignes = $requete->fetchAll();

/*if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !<br>";

    $requete->debugDumpParams();

    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}*/

?>

    <h1>Portail Admin</h1>

    <h3>Ajouter une nouvelle plage à l'étude</h3>

    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a href="listeEtudes.php" class="btn btn-danger"><i class="fal fa-long-arrow-left"></i> Retour</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Noms</th>
            <th>Actions</th>
        </tr>
        <?php
        foreach ($lignes as $ligne) {
            ?>
            <tr>

                <td><?php echo $ligne["nom"] ?></td>
                <td>
                    <a href="ajoutPlage2.php?idplage=<?php echo $ligne["idplage"] ?>&idetude=<?php echo $idetude ?>"
                       class="btn btn-success"><i class="fa fa-plus-square"></i></a>
                </td>
            </tr>

            <?php
        }

        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>