<?php
include "header.php";
mon_header("IFROCEAN - Login");
?>
    <br>
    <h5>LOGIN</h5>
    <br>
    <form action="Actions/actionLogin.php" method="post">
        <div class="form-group">
            <label for="idadmin">Identifiant</label>
            <input type="text" class="form-control" id="idadmin" name="idadmin">
            <small id="emailHelp" class="form-text text-muted">Vos identifiants ne seront pas massivement communiqués.</small>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>


<?php include "footer.php";
mon_footer(); ?>