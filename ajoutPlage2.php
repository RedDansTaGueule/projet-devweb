<?php

session_start();

include "header.php";
mon_header("IFROCEAN - Nouvelle Plage");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;

$idetude = filter_input(INPUT_GET, "idetude");
$idplage = filter_input(INPUT_GET,"idplage")
?>

    <h1>Nouvelle Plage</h1>

    <form method="post" action="actions/actionAjoutPlage.php">
        <input type="hidden" name="token" value="<?php echo $token ?>">

        <input type="hidden" name="idplage" value="<?php echo $idplage ?>">

        <input type="hidden" name="idetude" value="<?php echo $idetude ?>">

        <div class="form-group">
            <label for="superficie">Superficie</label>
            <input type="number" class="form-control" id="superficie" name="superficie" placeholder="Superficie de la plage" required>
        </div>

        <a href="ajoutPlage.php" class="btn btn-danger">
            <i class="fa fa-long-arrow-left"></i>
            Retour
        </a>

        <button type="submit" class="btn btn-primary pull-right">Ajouter à l'étude</button>
    </form>



<?php include "footer.php";
mon_footer(); ?>