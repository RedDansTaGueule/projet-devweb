<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Supprimer une étude");

$token = rand(0, 2000000000);
$_SESSION["token"] = $token;

$idplage = filter_input(INPUT_GET, "idplage");
require "config.php";

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select id, nom from plage where id=:idplage");

$requete->bindParam(":idplage", $idplage);
$requete->execute();
$lignes = $requete->fetchAll();

$nom = $lignes[0]["nom"];
?>

<h1>Supprimer une étude</h1>

    <h2>Etes-vous sûr de vouloir supprimer la plage <b><?php echo $nom ?></b> ?</h2>

<form method="post" action="actions/actionDelPlage.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="idplage" value="<?php echo $idplage ?>">

    <a href="listeEtudes.php" class="btn btn-primary">
        <i class="fa fa-long-arrow-left"></i>
        Retour</a>

    <button type="submit" class="btn btn-danger pull-right"><i class="fal fa-trash"></i> Supprimer</button>
</form>

<br>

<?php include "footer.php";
mon_footer(); ?>