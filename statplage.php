<?php
session_start();


$idetude=filter_input(INPUT_GET, "idetude");
$idplage=filter_input(INPUT_GET, "idplage");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select g.nomgrp, p.nom as plage, e.nom as etude, z.superficie, z.id as idzone  from prelevement pr
                                   join plage p on p.id=pr.idplage
                                   join etude e on e.id=pr.idetude
                                   join groupe g on g.id=pr.idgroupe
                                   join zone z on z.id=pr.idzone
                                   where e.id=:idetude and p.id=:idplage");

$requete->bindParam(":idetude", $idetude);
$requete->bindParam(":idplage", $idplage);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !<br>";

    $requete->debugDumpParams();

    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}

$etude=$lignes[0]["etude"];
$plage=$lignes[0]["plage"];

include "header.php";
mon_header("IFROCEAN - Etude ".$etude);

?>
    <h1>Portail Admin</h1>

    <h3>Statistiques de l'étude <b><?php echo $etude?></b> en fonction de la plage <b><?php echo $plage ?></b></h3>

    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a class="btn btn-danger" href="listeEtudes.php" role="button"><i class="fa fa-long-arrow-left"></i> Retour</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Groupe</th>
            <th>Superficie</th>
            <th>Actions</th>
        </tr>
        <?php


        foreach ($lignes as $ligne) {
            ?>
            <tr>
                <td><?php echo $ligne["nomgrp"] ?></td>
                <td><?php echo $ligne["superficie"] ?>m²</td>
                <td>
                    <a href="statZone.php?idetude=<?php echo $idetude ?>&idplage=<?php echo $idplage ?>&idzone=<?php echo $ligne["idzone"] ?>"
                       class="btn btn-success"><i class="fas fa-arrow-alt-square-right"></i></a>
                </td>
            </tr>

            <?php
        }

        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>