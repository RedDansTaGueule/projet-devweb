<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Modifier étude");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;

$idetude=filter_input(INPUT_GET, "idetude");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select id as idetude, nom, datedebut, datefin, nomcreateur from etude where id=:idetude");

$requete->bindParam(":idetude", $idetude);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !";
    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}

$idetude=$lignes[0]["idetude"];
$nom=$lignes[0]["nom"];
$datedebut=$lignes[0]["datedebut"];
$datefin=$lignes[0]["datefin"];
$nomcreateur=$lignes[0]["nomcreateur"];
?>

<h1>Modifier une étude</h1>

<form method="post" action="actions/actionEditEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="id" value="<?php echo $idetude ?>">
    <div class="form-group">
        <label for="nom">Nom de l'étude</label>
        <input type="text" class="form-control" id="nom" maxlength="50" name="nom" value="<?php echo $nom ?>" placeholder="Nom de l'étude" required>
    </div>

    <div class="form-group">
        <label for="datedebut">Date de début</label>
        <input type="date" class="form-control" id="datedebut" name="datedebut" value="<?php echo $datedebut ?>" placeholder="Date de début" required>
    </div>

    <?php
    if ($datefin) {
        echo '<div class="form-group">
                    <label for="datefin">Date de fin</label>
                    <input type="date" class="form-control" id="datedebut" name="datedebut" value="'.$datefin.'" placeholder="Date de fin">
              </div>';
    }

    ?>

    <div class="form-group">
        <label for="nomcreateur">Nom du créateur</label>
        <input type="text" class="form-control" id="nomcreateur" name="nomcreateur" value="<?php echo $nomcreateur ?>" placeholder="Nom du créateur" required>
    </div>

    <div class="modif">
        <button type="submit" class="btn btn-primary pull-right">Enregistrer modifications</button>
        <?php
        if (!$datefin) {
            echo '<a class="btn btn-danger" href="finirEtude.php?id='.$idetude.'" role="button">Finir l\'étude</a>';
        }
        ?>
    </div>

</form>

<br>

<?php include "footer.php";
mon_footer(); ?>
