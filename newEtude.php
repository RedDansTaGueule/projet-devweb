<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Créer étude");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;
?>

<h1>Création d'un étude</h1>

<form method="post" action="actions/actionNewEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">

    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" maxlength="50" name="nom" placeholder="Nom de l'étude" required>
    </div>
    
    <div class="form-group">
        <label for="datedebut">Date de début</label>
        <input type="date" class="form-control" id="datedebut" name="datedebut" placeholder="Date de début" required>
    </div>

    <div class="form-group">
        <label for="nomcreateur">Nom du créateur</label>
        <input type="text" class="form-control" id="nomcreateur" maxlength="50" name="nomcreateur" placeholder="Nom du créateur" required>
    </div>

    <a href="listeEtudes.php" class="btn btn-danger">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>

    <button type="submit" class="btn btn-primary pull-right">Créer étude</button>
</form>
