<?php
session_start();


$idetude=filter_input(INPUT_GET, "idetude");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select p.nom as plage, e.nom as etude, ep.superficie, ep.idetude as idetude, ep.idplage from plage p
                                   join etude_plage ep on p.id=ep.idplage
                                   join etude e on e.id=ep.idetude
                                   where e.id=:idetude");

$requete->bindParam(":idetude", $idetude);
$requete->execute();

$lignes=$requete->fetchAll();

/*if (count($lignes)>1) {
    echo "Cet id n'est pas valide !<br>";

    $requete->debugDumpParams();

    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}*/


if (count($lignes)==0) {
    include "header.php";
    mon_header("IFROCEAN - Etude");
    echo "<b> Il n'y a pas de plage dans cette étude </b> <br> <br>"
        ."<div class=\"portailAdmin\">"
        ."<div class=\"portailAdmin1\">"
            .'<a class="btn btn-warning" href="ajoutPlage.php?idetude='.$idetude.'" role="button"><i class="fa fa-plus-square"></i> Ajouter Plage</a>'
            ."<a class=\"btn btn-danger\" href=\"listeEtudes.php\" role=\"button\"><i class=\"fa fa-long-arrow-left\"></i> Retour</a>"
        ."</div>"
    ."</div>";
    include "footer.php";
    die();
}



$etude=$lignes[0]["etude"];

include "header.php";
mon_header("IFROCEAN - Etude ".$etude);

?>
    <h1>Portail Admin</h1>

    <h3>Liste des plages de l'étude <b><?php echo $etude?></b></h3>

    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a class="btn btn-warning" href="ajoutPlage.php?idetude=<?php echo "$idetude" ?>" role="button"><i class="fa fa-plus-square"></i> Ajouter Plage</a>
            <a class="btn btn-danger" href="listeEtudes.php" role="button"><i class="fa fa-long-arrow-left"></i> Retour</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Noms</th>
            <th>Superficie</th>
            <th>Actions</th>
        </tr>
        <?php


        foreach ($lignes as $ligne) {
            ?>
            <tr>
                <td><?php echo $ligne["plage"] ?></td>
                <td><?php echo $ligne["superficie"] ?>m²</td>
                <td>
                    <a href="modifierPlageEtude.php?idetude=<?php echo $ligne["idetude"] ?>&idplage=<?php echo $ligne["idplage"] ?>"
                       class="btn btn-primary"><i class="fa fa-pen"></i></a>
                    <a href="confirmDelPlageEtude.php?idetude=<?php echo $ligne["idetude"] ?>&idplage=<?php echo $ligne["idplage"] ?>"
                       class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>

            <?php
        }

        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>