<?php
include "header.php";
mon_header("IFORCEAN - Etudes");
?>

    <h1>Portail Admin</h1>

    <h3>Liste des études</h3>

    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a class="btn btn-primary" href="newEtude.php" role="button"><i class="fa fa-plus-square"></i>  Nouvelle étude</a>
            <a class="btn btn-warning" href="listePlages.php" role="button"><i class="fas fa-umbrella-beach"></i>  Plages</a>
        </div>
        <div class="portailAdmin2">
            <a class="btn btn-success" href="listeEspece.php" role="button"><i class="fas fa-wrench"></i>  Gérer espèces</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Nom</th>
            <th>Date de début</th>
            <th>Date de fin</th>
            <th>Nom du créateur</th>
            <th>Actions</th>
        </tr>
        <?php
        require "config.php";

        $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
        $requete = $db->prepare("select id as idetude, nom, datedebut, datefin, nomcreateur from etude");
        $requete->execute();
        $lignes = $requete->fetchAll();


        foreach ($lignes as $ligne) {
            ?>
            <tr>

                <td><?php echo $ligne["nom"] ?></td>
                <td><?php echo $ligne["datedebut"] ?></td>
                <td><?php echo $ligne["datefin"] ?></td>
                <td><?php echo $ligne["nomcreateur"] ?></td>
                <td>
                    <a href="modifierEtude.php?idetude=<?php echo $ligne["idetude"] ?>"
                       class="btn btn-primary"><i class="fa fa-pen"></i></a>
                    <a href="confirmDel.php?idetude=<?php echo $ligne["idetude"] ?>"
                       class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    <?php
                    if ($ligne["datefin"]) {?>
                        <a href="statistiques.php?idetude=<?php echo $ligne["idetude"] ?>"
                       class="btn btn-success"><i class="fas fa-analytics"></i></a>
                    <?php
                    }
                    else {?>
                        <a href="plageEtude.php?idetude=<?php echo $ligne["idetude"] ?>"
                        class="btn btn-warning"><i class="fas fa-umbrella-beach"></i></a>
                    <?php
                    }
                    ?>
                </td>
            </tr>

            <?php
        }
        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>