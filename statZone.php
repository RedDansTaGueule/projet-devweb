<?php
session_start();


$idetude=filter_input(INPUT_GET, "idetude");
$idplage=filter_input(INPUT_GET, "idplage");
$idzone=filter_input(INPUT_GET, "idzone");

require "config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("select g.nomgrp, p.nom as plage, e.nom as etude, z.superficie, z.lat1, z.lon1, z.lat2, z.lon2, z.lat3, z.lon3, z.lat4, z.lon4, sp.nmbresp, s.nom as espece
                                   from prelevement pr
                                   join plage p on p.id=pr.idplage
                                   join etude e on e.id=pr.idetude
                                   join groupe g on g.id=pr.idgroupe
                                   join zone z on z.id=pr.idzone
                                   join espece_prelevement sp on sp.idprelevement=pr.id
                                   join espece s on sp.idespece=s.id
                                   where e.id=:idetude and p.id=:idplage and z.id=:idzone");

$requete->bindParam(":idetude", $idetude);
$requete->bindParam(":idplage", $idplage);
$requete->bindParam(":idzone", $idzone);
$requete->execute();

$lignes=$requete->fetchAll();

if (count($lignes)!=1) {
    echo "Cet id n'est pas valide !<br>";

    $requete->debugDumpParams();

    http_response_code(404);
    include "footer.php";
    mon_footer();
    die();
}

$etude=$lignes[0]["etude"];
$plage=$lignes[0]["plage"];
$superficie=$lignes[0]["superficie"];
$lat1=$lignes[0]["lat1"];
$long1=$lignes[0]["lon1"];
$lat2=$lignes[0]["lat2"];
$long2=$lignes[0]["lon2"];
$lat3=$lignes[0]["lat3"];
$long3=$lignes[0]["lon3"];
$lat4=$lignes[0]["lat4"];
$long4=$lignes[0]["lon4"];
$nomgrp=$lignes[0]["nomgrp"];


include "header.php";
mon_header("IFROCEAN - Etude ".$etude);

?>
    <h1>Portail Admin</h1>

    <h3>Statistiques de l'étude <b><?php echo $etude?></b> en fonction de la plage <b><?php echo $plage ?></b> et de la zone gérée par le groupe <b><?php echo $nomgrp ?></b></h3>

    <p>Les 4 points définissant cette zone sont :<br>lat: <?php echo $lat1?>   long: <?php echo $long1 ?>
        <br>lat: <?php echo $lat2?>   long: <?php echo $long2 ?>
        <br>lat: <?php echo $lat3?>   long: <?php echo $long3 ?>
        <br>lat: <?php echo $lat4?>   long: <?php echo $long4 ?></p>
    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a class="btn btn-danger" href="listeEtudes.php" role="button"><i class="fa fa-long-arrow-left"></i> Retour</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Espèce</th>
            <th>Nombre</th>
        </tr>
        <?php


        foreach ($lignes as $ligne) {
            ?>
            <tr>
                <td><?php echo $ligne["espece"] ?></td>
                <td><?php echo $ligne["nmbresp"] ?></td>

            </tr>

            <?php
        }

        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>