<?php
include "header.php";
mon_header("Sélectionner un groupe");
session_start();
?>
<?php
require "config.php";


$idplage = filter_input(INPUT_POST, "idplage");
$_SESSION["idplage"] = $idplage;
//créer l'objet PDO qui me connecte à la bdd
$db = new PDO("mysql:host=" . config::SERVEUR . ";dbname=" . config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete = $db->prepare("select id, nomgrp, nmbrpers from groupe");

$requete->execute();

$groupes = $requete->fetchALL();


?>

<div class="jumbotron jumbotron-fluid">
    <div class="container3">
        <h1 class="display-4">Paramètres du prélèvement</h1>
    </div>
</div>
<div>
    <form action="PointDepartGPS.php" method="post">
        <label for="groupe">Sélectionnez un groupe</label>
        <select name="idgroupe" class="form-control">
            <?php
            foreach($groupes as $groupe){
                ?>
                <option value="<?php echo $groupe["id"] ?>">
                    <?php echo $groupe["nomgrp"] ?> - (<?php echo $groupe["nmbrpers"];
                    echo " personne";
                    if($groupe["nmbrpers"] != "1") {
                        echo "s";
                    }
                    ?>)
                </option>
                <?php
            }
            ?>
        </select>
        <input type="hidden" name="idplage" value="<?php echo $idplage ?>"/>
        <input type="submit" class="btn btn-info"/>
    </form>
</div>

<h4>Ou alors créer une équipe :</h4>

<form method="post" action="actions/actionAjout_Group.php">
    <div class="form-group">
        <input type="text" class="form-control" id="nomgrp" maxlength="50"
               name="nomgrp"
               placeholder="Nom de l'équipe..." required>
        <input type="text" class="form-control" id="nmbrpers" name="nmbrpers" placeholder="Nombre de personnes..."
               required>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
</form>




