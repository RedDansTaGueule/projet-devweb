<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Supprimer une étude");

$token = rand(0, 2000000000);
$_SESSION["token"] = $token;

$idetude = filter_input(INPUT_GET, "idetude");
require "config.php";

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $db->prepare("select id, nom, datedebut, datefin, nomcreateur from etude where id=:idetude");

$requete->bindParam("idetude", $idetude);
$requete->execute();
$lignes = $requete->fetchAll();

$nom = $lignes[0]["nom"];
$datedebut = $lignes[0]["datedebut"];
$datefin = $lignes[0]["datefin"];
$nomcreateur = $lignes[0]["nomcreateur"];
?>

<h1>Supprimer une étude</h1>

    <h2>Etes-vous sûr de vouloir supprimer l'étude <b><?php echo $nom ?></b> créée par <b><?php echo $nomcreateur ?></b> ?</h2>

<form method="post" action="actions/actionDelEtude.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">
    <input type="hidden" name="id" value="<?php echo $idetude ?>">

    <a href="listeEtudes.php" class="btn btn-primary">
        <i class="fa fa-long-arrow-left"></i>
        Retour</a>

    <button type="submit" class="btn btn-danger pull-right"><i class="fal fa-trash"></i> Supprimer</button>
</form>

<br>

<?php include "footer.php";
mon_footer(); ?>