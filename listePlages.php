<?php
include "header.php";
mon_header("IFORCEAN - Plages");
?>

    <h1>Portail Admin</h1>

    <h3>Liste des plages</h3>

    <br>
    <div class="portailAdmin">
        <div class="portailAdmin1">
            <a class="btn btn-primary" href="newPlage.php" role="button"><i class="fa fa-plus-square"></i>  Nouvelle Plage</a>
            <a class="btn btn-warning" href="listeEtudes.php" role="button"><i class="fas fa-search"></i> Études</a>
        </div>
        <div class="portailAdmin2">
            <a class="btn btn-success" href="listeEspece.php" role="button"><i class="fas fa-wrench"></i>  Gérer espèces</a>
        </div>
    </div>
    <br>
    <br>
    <table class="table">
        <tr>
            <th>Noms</th>
            <th>Actions</th>
        </tr>
        <?php
        require "config.php";

        $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
        $requete = $db->prepare("select id, nom from plage");
        $requete->execute();
        $lignes = $requete->fetchAll();

        foreach ($lignes as $ligne) {
            ?>
            <tr>

                <td><?php echo $ligne["nom"] ?></td>
                <td>
                    <a href="modifierPlage.php?id=<?php echo $ligne["id"] ?>"
                       class="btn btn-primary"><i class="fa fa-pen"></i></a>
                    <a href="confirmDelPlage.php?idplage=<?php echo $ligne["id"] ?>"
                       class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>

            <?php
        }

        ?>
    </table>



<?php include "footer.php";
mon_footer(); ?>