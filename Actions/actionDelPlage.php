<?php
session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal.";
    die();
}
$idplage = filter_input(INPUT_POST, "idplage");

require "../config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("delete from plage where id=:idplage");
$requete->bindParam(":idplage", $idplage);
$requete->execute();
$requete->debugDumpParams();

header("location: ../listePlages.php");