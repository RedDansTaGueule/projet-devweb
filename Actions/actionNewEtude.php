<?php
session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal !";
    die();
}

$nom = filter_input(INPUT_POST, "nom");
$datedebut = filter_input(INPUT_POST, "datedebut");
$nomcreateur = filter_input(INPUT_POST, "nomcreateur");

require "../config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("insert into etude(nom, datedebut, nomcreateur) values (:nom, :datedebut, :nomcreateur)");

$requete->bindParam(":nom", $nom);
$requete->bindParam(":datedebut", $datedebut);
$requete->bindParam(":nomcreateur", $nomcreateur);

$requete->execute();
$requete->debugDumpParams();

header("location: ../listeEtudes.php");