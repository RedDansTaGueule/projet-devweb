<?php

session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal !";
    die();
}

$id = filter_input(INPUT_POST, "id");
$nom = filter_input(INPUT_POST, "nom");
$datedebut = filter_input(INPUT_POST, "datedebut");
$datefin = filter_input(INPUT_POST, "datefin");
$nomcreateur = filter_input(INPUT_POST, "nomcreateur");

require "../config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

if ($datefin) {
    $requete = $db->prepare("update etude set nom=:nom, datedebut=:datedebut, datefin=:datefin, nomcreateur=:nomcreateur where id=:id");
    $requete->bindParam("datefin", $datefin);
}
else {
    $requete = $db->prepare("update etude set nom=:nom, datedebut=:datedebut, nomcreateur=:nomcreateur where id=:id");

}
$requete->bindParam(":id", $id);
$requete->bindParam(":nom", $nom);
$requete->bindParam("datedebut", $datedebut);
$requete->bindParam("nomcreateur", $nomcreateur);
$requete->execute();
$requete->debugDumpParams();

header("location: ../listeEtudes.php");