<?php

session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal !";
    die();
}

$idetude = filter_input(INPUT_POST, "idetude");
$idplage = filter_input(INPUT_POST, "idplage");
$superficie = filter_input(INPUT_POST, "superficie");

require "../config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("update etude_plage set superficie=:superficie where idplage=:idplage AND idetude=:idetude");

$requete->bindParam(":idetude", $idetude);
$requete->bindParam(":idplage", $idplage);
$requete->bindParam(":superficie", $superficie);
$requete->execute();
$requete->debugDumpParams();

header("location: ../listeEtudes.php");