<?php
session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal.";
    die();
}

$idetude = filter_input(INPUT_POST, "idetude");
$idplage = filter_input(INPUT_POST, "idplage");

require "../config.php";

$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("delete from etude_plage where idplage=:idplage AND idetude=:idetude");
$requete->bindParam(":idplage", $idplage);
$requete->bindParam(":idetude", $idetude);
$requete->execute();
$requete->debugDumpParams();

header("location: ../listeEtudes.php");