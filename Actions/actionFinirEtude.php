<?php
session_start();

$tokenEnvoye = filter_input(INPUT_POST, "token");
if ($tokenEnvoye != $_SESSION["token"]) {
    echo "Le piratage, c'est mal !";
    die();
}

$id = filter_input(INPUT_POST,"id");
$datefin = filter_input(INPUT_POST,"datefin");

require "../config.php";
$db = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE );

$requete = $db->prepare("update etude set datefin=:datefin where id=:id");
$requete->bindParam(":id", $id);
$requete->bindParam(":datefin", $datefin);
$requete->execute();
$requete->debugDumpParams();

header("location: ../listeEtudes.php");