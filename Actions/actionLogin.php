<?php
$idadmin = filter_input(INPUT_POST, "idadmin");
$password = filter_input(INPUT_POST, "password");


require "../config.php";

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $db->prepare("select * from administrateurs");
$requete->execute();
$lignes = $requete->fetchAll();

foreach ($lignes as $ligne) {
    if ($idadmin == $ligne["pseudo"] & $password == $ligne["password"]) {
        header("location: ../listeEtudes.php");
    }
}
include "../header.php";
mon_header("IFROCEAN - Login");
echo "Identifiants invalides !";
echo "<br> <br><a href='../login.php' class='btn btn-danger'><i class='fal fa-long-arrow-left'></i> Retour</a>";

