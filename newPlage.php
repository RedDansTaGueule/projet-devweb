<?php
session_start();

include "header.php";
mon_header("IFROCEAN - Nouvelle Plage");

$token=rand(0,2000000000);
$_SESSION["token"]=$token;
?>

<h1>Nouvelle Plage</h1>

<form method="post" action="actions/actionNewPlage.php">
    <input type="hidden" name="token" value="<?php echo $token ?>">

    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" maxlength="50" name="nom" placeholder="Nom de la plage" required>
    </div>

    <a href="listeEtudes.php" class="btn btn-danger">
        <i class="fal fa-long-arrow-left"></i>
        Retour
    </a>

    <button type="submit" class="btn btn-primary pull-right">Ajouter plage</button>
</form>
