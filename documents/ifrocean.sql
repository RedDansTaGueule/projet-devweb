-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 20 jan. 2020 à 11:50
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ifrocean`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateurs`
--

CREATE TABLE `administrateurs` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `administrateurs`
--

INSERT INTO `administrateurs` (`id`, `pseudo`, `password`) VALUES
(1, 'jdup', 'useros'),
(2, 'pdup', 'adminos');

-- --------------------------------------------------------

--
-- Structure de la table `espece`
--

CREATE TABLE `espece` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `espece`
--

INSERT INTO `espece` (`id`, `nom`) VALUES
(4, 'Ver de terre 01'),
(5, 'Ver de terre 02'),
(6, 'Ver de terre 03');

-- --------------------------------------------------------

--
-- Structure de la table `espece_prelevement`
--

CREATE TABLE `espece_prelevement` (
  `id` int(11) NOT NULL,
  `idespece` int(11) NOT NULL,
  `idprelevement` int(11) NOT NULL,
  `nmbresp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `espece_prelevement`
--

INSERT INTO `espece_prelevement` (`id`, `idespece`, `idprelevement`, `nmbresp`) VALUES
(7, 6, 14, 15);

-- --------------------------------------------------------

--
-- Structure de la table `etude`
--

CREATE TABLE `etude` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `datedebut` date NOT NULL,
  `datefin` date DEFAULT NULL,
  `nomcreateur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etude`
--

INSERT INTO `etude` (`id`, `nom`, `datedebut`, `datefin`, `nomcreateur`) VALUES
(11, 'Compter les vers 1', '2020-01-20', '2020-01-24', 'Pierre Dupont'),
(12, 'Compter les vers 2', '2020-01-15', NULL, 'Jean Dupont'),
(13, 'Compter les requêtes SQL', '2002-04-24', NULL, 'Le Roi du SQL');

-- --------------------------------------------------------

--
-- Structure de la table `etude_plage`
--

CREATE TABLE `etude_plage` (
  `idetude` int(11) NOT NULL,
  `idplage` int(11) NOT NULL,
  `superficie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etude_plage`
--

INSERT INTO `etude_plage` (`idetude`, `idplage`, `superficie`) VALUES
(11, 7, 145),
(12, 8, 458);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `nomgrp` varchar(30) NOT NULL,
  `nmbrpers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nomgrp`, `nmbrpers`) VALUES
(5, 'Equipe 1', 4),
(6, 'Equipe 2', 1),
(7, 'Equipe 3', 2);

-- --------------------------------------------------------

--
-- Structure de la table `plage`
--

CREATE TABLE `plage` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `plage`
--

INSERT INTO `plage` (`id`, `nom`) VALUES
(6, 'Sable 1'),
(7, 'Sable 2'),
(8, 'Galets'),
(9, 'Gravier'),
(10, 'Herbe');

-- --------------------------------------------------------

--
-- Structure de la table `prelevement`
--

CREATE TABLE `prelevement` (
  `id` int(11) NOT NULL,
  `idplage` int(11) DEFAULT NULL,
  `idetude` int(11) DEFAULT NULL,
  `idgroupe` int(11) DEFAULT NULL,
  `idzone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `prelevement`
--

INSERT INTO `prelevement` (`id`, `idplage`, `idetude`, `idgroupe`, `idzone`) VALUES
(14, 7, 11, 5, 24);

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `lat1` float NOT NULL,
  `lon1` float NOT NULL,
  `lat2` float NOT NULL,
  `lon2` float NOT NULL,
  `lat3` float NOT NULL,
  `lon3` float NOT NULL,
  `lat4` float NOT NULL,
  `lon4` float NOT NULL,
  `superficie` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `zone`
--

INSERT INTO `zone` (`id`, `lat1`, `lon1`, `lat2`, `lon2`, `lat3`, `lon3`, `lat4`, `lon4`, `superficie`) VALUES
(24, 47.2061, -1.53929, 48.2061, -0.539293, 48.2041, -0.539893, 48.2068, -0.539493, 54),
(25, 47.2061, -1.53935, 48.2061, -0.539347, 48.2064, -0.539349, 48.2068, -0.539334, 67);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `espece`
--
ALTER TABLE `espece`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `espece_prelevement`
--
ALTER TABLE `espece_prelevement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prelevement` (`idprelevement`),
  ADD KEY `fk_espece` (`idespece`);

--
-- Index pour la table `etude`
--
ALTER TABLE `etude`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etude_plage`
--
ALTER TABLE `etude_plage`
  ADD PRIMARY KEY (`idetude`,`idplage`),
  ADD KEY `idplage` (`idplage`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plage`
--
ALTER TABLE `plage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prelevement`
--
ALTER TABLE `prelevement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_plage` (`idplage`),
  ADD KEY `fk_etude` (`idetude`) USING BTREE,
  ADD KEY `fk_groupe` (`idgroupe`),
  ADD KEY `fk_zone` (`idzone`) USING BTREE;

--
-- Index pour la table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `administrateurs`
--
ALTER TABLE `administrateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `espece`
--
ALTER TABLE `espece`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `espece_prelevement`
--
ALTER TABLE `espece_prelevement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `etude`
--
ALTER TABLE `etude`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `plage`
--
ALTER TABLE `plage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `prelevement`
--
ALTER TABLE `prelevement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `espece_prelevement`
--
ALTER TABLE `espece_prelevement`
  ADD CONSTRAINT `fk_espece` FOREIGN KEY (`idespece`) REFERENCES `espece` (`id`),
  ADD CONSTRAINT `fk_prelevement` FOREIGN KEY (`idprelevement`) REFERENCES `prelevement` (`id`);

--
-- Contraintes pour la table `etude_plage`
--
ALTER TABLE `etude_plage`
  ADD CONSTRAINT `etude_plage_ibfk_1` FOREIGN KEY (`idetude`) REFERENCES `etude` (`id`),
  ADD CONSTRAINT `etude_plage_ibfk_2` FOREIGN KEY (`idplage`) REFERENCES `plage` (`id`);

--
-- Contraintes pour la table `prelevement`
--
ALTER TABLE `prelevement`
  ADD CONSTRAINT `fk_etude` FOREIGN KEY (`idetude`) REFERENCES `etude` (`id`),
  ADD CONSTRAINT `fk_groupe` FOREIGN KEY (`idgroupe`) REFERENCES `groupe` (`id`),
  ADD CONSTRAINT `fk_plage` FOREIGN KEY (`idplage`) REFERENCES `plage` (`id`),
  ADD CONSTRAINT `fk_zone` FOREIGN KEY (`idzone`) REFERENCES `zone` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
