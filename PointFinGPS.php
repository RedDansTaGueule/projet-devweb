<?php
include "header.php";
mon_header("2 derniers points GPS");
session_start();
?>
<?php
require "config.php";

$idgroupe = filter_input(INPUT_POST, "idgroupe");
$_SESSION["idgroupe"] = $idgroupe;


?>
<div class="jumbotron jumbotron-fluid">
    <div class="container3">
        <h1 class="display-4">Coordonnées</h1>
    </div>
</div>
<div class="nono">
    <button type="button" id="find-me" class="btn btn-success">Obtenir mes coordonnées :<br><i>(cliquez à nouveau lors de la prise du point
            n°4)</i></button>
    <br/>
    <p id="status"></p>
    <a id="map-link" target="_blank"></a>
</div>
<script>
    function geoFindMe() {

        const status = document.querySelector('#status');
        const mapLink = document.querySelector('#map-link');
        const input_latitude1 = document.querySelector("#input_latitude3");
        const input_longitude1 = document.querySelector("#input_longitude3");
        const input_latitude2 = document.querySelector("#input_latitude4");
        const input_longitude2 = document.querySelector("#input_longitude4");

        mapLink.href = '';
        mapLink.textContent = '';

        function success(position) {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;

            status.textContent = '';
            mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude}/${longitude}`;
            mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
            if(input_latitude3.value == "" || input_longitude3.value == "") {
                input_latitude3.value = latitude;
                input_longitude3.value = longitude;
            } else {
                input_latitude4.value = latitude;
                input_longitude4.value = longitude;
            }

        }

        function error() {
            status.textContent = 'Unable to retrieve your location';
        }

        if (!navigator.geolocation) {
            status.textContent = 'Geolocation is not supported by your browser';
        } else {
            status.textContent = 'Locating…';
            navigator.geolocation.getCurrentPosition(success, error);
        }

    }

    document.querySelector('#find-me').addEventListener('click', geoFindMe);
</script>


<form action="actions/actionAjoutZone2.php" method="post">
    <h4>
        <b>Coordonnées en Décimaux :</b>
    </h4>
    <br>
    <h5 class="titrePoint">Point numéro 3 :</h5>
    <div class="form">
        <div class="form-group">
            <label class="lat3" for="latitude">Latitude</label>
            <input class="form-control" id="input_latitude3" name="lat3" value="">
        </div>
    </div>

    <div class="form">
        <div class="form-group">
            <label class="lon3" for="longitude">Longitude</label>
            <input class="form-control" id="input_longitude3" name="lon3" value="">
        </div>
    </div>
    <br><br>
    <h5 class="titrePoint">Point numéro 4 :</h5>

    <div class="form">
        <div class="form-group">
            <label class="titreLong" for="latitude">Latitude</label>
            </label>
            <input class="form-control" id="input_latitude4"  name="lat4" value="">
        </div>
    </div>
    </div>
    <div class="form">
        <div class="form-group"><label class="titreLong" for="longitude">Longitude</label>
            </label>
            <input class="form-control" id="input_longitude4"  name="lon4" value="">
        </div>
    </div>
    <br><br>
    <div class="submt">
        <button type="submit" class="btn btn-primary">Enregistrer les points</button>
    </div>
</form>
