<?php
include "header.php";
mon_header("Choisir une étude :");
session_start();
?>
<?php
require "config.php";
//créer l'objet PDO qui me connecte à la bdd
$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete_etude=$db->prepare("select id, nom from etude");

$requete_etude->execute();

$etudes =$requete_etude->fetchALL();

$requete_plage=$db->prepare("select id, nom from plage");

$requete_plage->execute();

$plages =$requete_plage->fetchALL();


?>

<form action="actions/actionAjoutPrelevement.php" method="post" class="form-group">
    <label for="etude">Sélectionnez une etude</label>
    <select name="idetude" class="form-control">
        <?php
        foreach($etudes as $etude){
            ?>
            <option value="<?php echo $etude["id"] ?>">
                <?php echo $etude["nom"] ?>
            </option>
            <?php
        }
        ?>
    </select>
    <input type="submit"/>
</form>
