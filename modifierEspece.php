<?php
include "header.php";
mon_header("Modifier le nom de l'espèce");

//Récupérer l'id
$id=filter_input(INPUT_GET, "id");
require "config.php";
//créer l'objet PDO qui me connecte à la bdd
$db = new PDO( "mysql:host=".config::SERVEUR.";dbname=".config::BASE, config::UTILISATEUR, config::MOTDEPASSE);

$requete=$db->prepare("select id, nom from espece where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$lignes =$requete->fetchALL();
$nom=$lignes[0]["nom"];

?>
<h1>Modifier les informations du client</h1>

<form method="post" action="actions/actionModificationEspèces.php">
    <input type="hidden" name="id" value="<?php echo $id?>">
    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" maxlength="50"
               name="nom" value="<?php echo $nom ?>"
               placeholder="Nom..." required>

    <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
</form>
<?php
include 'footer.php';
mon_footer();
?>
